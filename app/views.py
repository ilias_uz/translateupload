# -*- coding: utf-8 -*- 
# Created by Iliyaz Kazikhodzhaev
# Import Django core utils
from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login as login_user
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from django import forms

# Import base function and Classe
from .forms import UploadFileForm
from .utils import handle_uploaded_file

# Import model for save translated text
from .models import TranslatedWords

# Import translator backeg
from googletrans import Translator
translator = Translator()


# Translation language dict
TRANLATOR_LANGUAGES = {
	'Russian': 'ru',
	'English': 'en',
	'Uzbek': 'uz',
	'Turkish': 'tk',
}


# Upload any type file to media folder in project unlimited until 20MB
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES['file'].size < int(settings.MAX_UPLOAD_MEDIA_SIZE):
                handle_uploaded_file(request.FILES['file'], request.FILES['file'].name)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError("only 20 MB photo and JPG, PNG is allowed")
    else:
        form = UploadFileForm()
    return render(request, 'profile.html', {'form': form})


# Upload just image and just 'JPG', 'PNG', 'jpg', 'png' types unlimited until 5MB
def upload_img(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            img_name = request.FILES['file'].name
            if request.FILES['file'].size < int(settings.MAX_UPLOAD_PHOTO_SIZE) and img_name.split('.')[1] in ['JPG', 'PNG', 'jpg', 'png']:
                save_path = 'images/'+img_name
                handle_uploaded_file(request.FILES['file'], save_path)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError("only 5 MB photo and JPG, PNG is allowed")
    return render(request, 'profile.html')


# Translate and save translated text in to database
def translate_text(request):
    if request.method == 'POST':
    	translate_content = request.POST.get('translate_content')
    	if translate_content == '':
    		return render(request, 'profile.html', {'translate_error': 'Pleas type the text!'})
    	translate_from = request.POST.get('translate_from')
    	translate_to = request.POST.get('translate_to')
    	trns = translator.translate(translate_content, dest=translate_to, src=translate_from)
    	TranslatedWords.objects.create(translate_from=translate_from,
    		translate_to=translate_to, sourse_word=translate_content, translated_word=trns.text)
    	return render(request, 'profile.html', {'translate_result': trns.text, 'translate_content': translate_content})
    else:
    	return render(request, 'profile.html', {'translate_error': 'Some think go wrong!'})


# log out end redirect login page
def logout_view(request):
    logout(request)
    return redirect('/')


''' check user authetification if user authenticated will open main page for transalte 
    and upload files else will return login page'''
def profile(request):
	if not request.user.is_authenticated:
		return redirect('/login')
	return render(request, 'profile.html')

	
# Login page for authetification
def login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login_user(request, user)
            return redirect('/')
        else:
            # Return a 'disabled account' error message
            return render(request, 'login.html', {'error': 'Pleas check username or password!'})
    else:
    	return render(request, 'login.html', {'error': 'Login error! Pleas check username and password'})
    return request(request, 'login.html')

