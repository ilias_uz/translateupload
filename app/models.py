from django.db import models

# Create your models here.
class TranslatedWords(models.Model):
	translate_from = models.CharField(max_length=12)
	translate_to = models.CharField(max_length=12)
	sourse_word = models.TextField()
	translated_word = models.TextField()

	def __str__(self):
		return 'Translated:%s, From:%s, To:%s, Ressult:%s' % (self.sourse_word, self.translate_from, 
			self.translate_to, self.translated_word)