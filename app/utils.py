# Saved upload file to directory function
def handle_uploaded_file(f, fname):
    path = 'media/%s' % fname
    with open(path, 'wb+') as destination:
        print(f)
        for chunk in f.chunks():
            print(chunk)
            destination.write(chunk)